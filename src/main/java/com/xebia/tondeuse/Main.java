package com.xebia.tondeuse;

import com.xebia.tondeuse.domain.model.InstructionsFile;
import com.xebia.tondeuse.domain.model.Mower;
import com.xebia.tondeuse.domain.model.MowerInstruction;
import com.xebia.tondeuse.infrastructure.FileDAO;

import java.io.IOException;
import java.util.List;

public class Main {

  private static String defaultFilePath = "initialization_file.txt";

  public static void main(String[] args) throws IOException {

    final String commandLineArgument = String.join("", args);
    if(!commandLineArgument.isEmpty()) {
      defaultFilePath = commandLineArgument;
    }

    final FileDAO fileDAO = new FileDAO();

    final InstructionsFile instructionsFile = fileDAO.read(defaultFilePath);
    final List<MowerInstruction> mowerInstructions = instructionsFile.getCommands();

    for (MowerInstruction mowerInstruction : mowerInstructions) {
      final Mower mower = new Mower(instructionsFile.getFieldDimensions(), mowerInstruction.getInitialPosition());
      System.out.println(mower.execute(mowerInstruction.getCommands()));
    }
  }
}
