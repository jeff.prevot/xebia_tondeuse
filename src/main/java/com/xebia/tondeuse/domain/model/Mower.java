package com.xebia.tondeuse.domain.model;

import com.xebia.tondeuse.dto.Position;

import java.util.Arrays;
import java.util.List;
import java.util.ListIterator;

public class Mower {

  private static final List<ORIENTATION> ORIENTATIONS = Arrays.asList(ORIENTATION.N, ORIENTATION.E, ORIENTATION.S, ORIENTATION.W);
  private static final Integer STEP = 1;
  private FieldDimensions fieldDimensions;
  private Position position;

  public Mower(final FieldDimensions fieldDimensions, final Position initialPosition) {
    this.fieldDimensions = fieldDimensions;
    this.position = initialPosition;
  }

  public Position execute(final List<COMMAND> commands) {
    for (final COMMAND command : commands) {
      executeCommand(command);
    }
    return this.position;
  }

  private void executeCommand(final COMMAND command) {
    switch (command) {
      case A:
        oneStep();
        break;
      case D:
        turnRight();
        break;
      case G:
        turnLeft();
        break;
      default:
        throw new UnsupportedOperationException();
    }
  }

  private void oneStep() {
    switch (position.getOrientation()) {
      case N:
        goNorth();
        break;
      case E:
        goEast();
        break;
      case S:
        goSouth();
        break;
      case W:
        goWest();
        break;
      default:
        throw new UnsupportedOperationException();
    }
  }

  private void turnRight() {
    final ListIterator<ORIENTATION> orientationListIterator = ORIENTATIONS.listIterator(ORIENTATIONS.indexOf(position.getOrientation()));
    orientationListIterator.next();
    final ORIENTATION finalOrientation = orientationListIterator.hasNext() ? orientationListIterator.next() : ORIENTATIONS.get(0);
    position.setOrientation(finalOrientation);
  }

  private void turnLeft() {
    final ListIterator<ORIENTATION> orientationListIterator = ORIENTATIONS.listIterator(ORIENTATIONS.indexOf(position.getOrientation()));
    final ORIENTATION finalOrientation = orientationListIterator.hasPrevious() ? orientationListIterator.previous() : ORIENTATIONS.get(ORIENTATIONS.size() - 1);
    position.setOrientation(finalOrientation);
  }

  private void goNorth() {
    if (this.position.getY() + STEP <= fieldDimensions.getHeight()) {
      this.position.setY(this.position.getY() + STEP);
    }
  }

  private void goSouth() {
    if (this.position.getY() - STEP >= 0) {
      this.position.setY(this.position.getY() - STEP);
    }
  }

  private void goWest() {
    if (this.position.getX() - STEP >= 0) {
      this.position.setX(this.position.getX() - STEP);
    }
  }

  private void goEast() {
    if (this.position.getX() + STEP <= fieldDimensions.getWidth()) {
      this.position.setX(this.position.getX() + STEP);
    }
  }
}
