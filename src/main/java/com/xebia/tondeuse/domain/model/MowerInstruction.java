package com.xebia.tondeuse.domain.model;

import com.xebia.tondeuse.dto.Position;

import java.util.List;

public class MowerInstruction {

  private Position initialPosition;
  private List<COMMAND> commands;

  public MowerInstruction(final Position initialPosition, final List<COMMAND> commands) {
    this.initialPosition = initialPosition;
    this.commands = commands;
  }

  public Position getInitialPosition() {
    return initialPosition;
  }

  public List<COMMAND> getCommands() {
    return commands;
  }
}
