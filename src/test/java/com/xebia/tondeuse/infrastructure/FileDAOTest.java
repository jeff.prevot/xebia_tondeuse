package com.xebia.tondeuse.infrastructure;

import com.xebia.tondeuse.domain.model.COMMAND;
import com.xebia.tondeuse.domain.model.FieldDimensions;
import com.xebia.tondeuse.domain.model.InstructionsFile;
import com.xebia.tondeuse.domain.model.MowerInstruction;
import com.xebia.tondeuse.domain.model.ORIENTATION;
import com.xebia.tondeuse.dto.Position;
import org.junit.jupiter.api.Test;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Arrays;
import java.util.Collections;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatThrownBy;

class FileDAOTest {

  private FileDAO fileDAO = new FileDAO();

  @Test
  void cannot_read_file_because_it_does_not_exist() {
    assertThatThrownBy(() -> fileDAO.read("no_file_here_path")).isInstanceOf(FileNotFoundException.class);
  }

  @Test
  void throw_exception_because_bad_zone_format() {
    assertThatThrownBy(() -> fileDAO.read("bad_zone_size_format.txt")).isInstanceOf(IllegalArgumentException.class)
                                                                      .hasMessage("File format error");
  }

  @Test
  void throw_exception_because_bad_initial_position_format() {
    assertThatThrownBy(() -> fileDAO.read("bad_initial_position.txt")).isInstanceOf(IllegalArgumentException.class)
                                                                      .hasMessage("File format error");
  }

  @Test
  void throw_exception_because_bad_initial_orientation() {
    assertThatThrownBy(() -> fileDAO.read("bad_initial_orientation.txt")).isInstanceOf(IllegalArgumentException.class)
                                                                         .hasMessage("File format error");
  }

  @Test
  void throw_exception_because_bad_command() {
    assertThatThrownBy(() -> fileDAO.read("bad_command.txt")).isInstanceOf(IllegalArgumentException.class)
                                                             .hasMessage("File format error");
  }

  @Test
  void one_mower() throws IOException {
    final InstructionsFile expectedInstructionsFile =
        new InstructionsFile(new FieldDimensions(2, 2),
                             Collections.singletonList(
                                 new MowerInstruction(
                                     new Position(0, 0, ORIENTATION.N),
                                     Arrays.asList(COMMAND.A, COMMAND.A, COMMAND.A, COMMAND.D, COMMAND.A, COMMAND.A, COMMAND.A, COMMAND.G, COMMAND.A, COMMAND.D, COMMAND.D, COMMAND.A, COMMAND.A, COMMAND.A, COMMAND.D, COMMAND.A, COMMAND.A, COMMAND.A, COMMAND.D))));
    final InstructionsFile instructionsFile = fileDAO.read("out_but_initial_position.txt");

    assertThat(instructionsFile).usingRecursiveComparison().isEqualTo(expectedInstructionsFile);
  }

  @Test
  void read_two_mowers_file() throws IOException {
    final InstructionsFile expectedInstructionsFile =
        new InstructionsFile(new FieldDimensions(5, 5),
                             Arrays.asList(
                                 new MowerInstruction(
                                     new Position(1, 2, ORIENTATION.N),
                                     Arrays.asList(COMMAND.G, COMMAND.A, COMMAND.G, COMMAND.A, COMMAND.G, COMMAND.A, COMMAND.G, COMMAND.A, COMMAND.A)),
                                 new MowerInstruction(
                                     new Position(3, 3, ORIENTATION.E),
                                     Arrays.asList(COMMAND.A, COMMAND.A, COMMAND.D, COMMAND.A, COMMAND.A, COMMAND.D, COMMAND.A, COMMAND.D, COMMAND.D, COMMAND.A))));

    final InstructionsFile instructionsFile = fileDAO.read("two_mowers.txt");

    assertThat(instructionsFile).usingRecursiveComparison().isEqualTo(expectedInstructionsFile);
  }
}